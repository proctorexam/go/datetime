package datetime

import (
	"fmt"
	"strconv"
	"time"
)

// Layout ISO 8601 with time yyyy-mm-dd HH:mm:ss
const ISO601 = "2006-01-02 15:04:05"

// Layout HH:mm:ss
const Sexagesimal = "15:04:05"

func FormatSexagesimal(duration time.Duration) string {
	if duration < time.Second {
		return "00:00:00"
	}
	secs := int(duration.Seconds())
	return fmt.Sprintf("%02v:%02v:%02v", secs/3600, (secs/60)%60, secs%60)
}

func ParseSexagesimal(input string) (duration time.Duration, err error) {
	var hours, mins, secs int
	if _, err = fmt.Sscanf(input, "%v:%v:%v", &hours, &mins, &secs); err != nil {
		return
	}
	duration = time.Duration(hours)*time.Hour + time.Duration(mins)*time.Minute + time.Duration(secs)*time.Second
	return
}

func ParseUnix(input string) (t time.Time, err error) {
	timeInt, err := strconv.ParseInt(input, 10, 64)

	switch len(input) {
	case 13:
		// Convert to seconds if input is in millis.
		t = time.Unix(timeInt/1000, 0).UTC()
	case 10:
		t = time.Unix(timeInt, 0).UTC()
	default:
		err = fmt.Errorf("input has invalid length: %v", len(input))
	}

	return
}

func FormatUnix(t time.Time) string {
	return strconv.Itoa(int(t.Unix()))
}
