package datetime_test

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/proctorexam/go/datetime"
)

func TestFormatSexagesimal(t *testing.T) {
	type args struct {
		d time.Duration
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "returns zeros anythign for less than a second",
			args: args{d: -time.Hour},
			want: "00:00:00",
		},
		{
			name: "formats hours into time string formatted as HH:MM:SS",
			args: args{d: time.Hour},
			want: "01:00:00",
		},
		{
			name: "formats minutes into time string formatted as HH:MM:SS",
			args: args{d: time.Minute * 23},
			want: "00:23:00",
		},
		{
			name: "formats minutes into time string formatted as HH:MM:SS",
			args: args{d: time.Second * 23},
			want: "00:00:23",
		},
		{
			name: "formats correctly for values bigger than 24h",
			args: args{d: 1024*time.Hour + 59*time.Minute + time.Second},
			want: "1024:59:01",
		},
		{
			name: "formats correctly for values bigger than 24h 2",
			args: args{d: 256 * time.Hour},
			want: "256:00:00",
		},
		{
			name: "use same format for values smaller than 1h",
			args: args{d: 11*time.Minute + 11*time.Second},
			want: "00:11:11",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := datetime.FormatSexagesimal(tt.args.d); got != tt.want {
				t.Errorf("FormatSexagesimal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseSexagesimal(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name         string
		args         args
		wantDuration time.Duration
		wantErr      bool
	}{
		{
			name:         "returns negative",
			wantDuration: -time.Hour,
			args:         args{input: "-01:00:00"},
		},
		{
			name:         "returns zeros",
			wantDuration: 0,
			args:         args{input: "00:00:00"},
		},
		{
			name:         "formats hours into time string formatted as HH:MM:SS",
			wantDuration: time.Hour,
			args:         args{input: "01:00:00"},
		},
		{
			name:         "formats minutes into time string formatted as HH:MM:SS",
			wantDuration: time.Minute * 23,
			args:         args{input: "00:23:00"},
		},
		{
			name:         "formats minutes into time string formatted as HH:MM:SS",
			wantDuration: time.Second * 23,
			args:         args{input: "00:00:23"},
		},
		{
			name:         "formats correctly for values bigger than 24h",
			wantDuration: 1024*time.Hour + 59*time.Minute + time.Second,
			args:         args{input: "1024:59:01"},
		},
		{
			name:         "use same format for values smaller than 1h",
			wantDuration: 11*time.Minute + 11*time.Second,
			args:         args{input: "00:11:11"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotDuration, err := datetime.ParseSexagesimal(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseSexagesimal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil {
				return
			}
			if gotDuration != tt.wantDuration {
				t.Errorf("ParseSexagesimal() = %v, want %v", gotDuration, tt.wantDuration)
			}
		})
	}
}

func TestISO601(t *testing.T) {
	wantFormat := "2022-01-02 10:04:59"
	got, err := time.Parse(datetime.ISO601, wantFormat)
	want := time.Date(2022, 1, 2, 10, 4, 59, 0, time.UTC)
	if err != nil {
		t.Error(err)
	} else if got != want {
		t.Errorf("time.Parse(datetime.ISO601) = %v, want %v", got, want)
	}

	gotFormat := got.Format(datetime.ISO601)
	if gotFormat != wantFormat {
		t.Errorf("time.Format(datetime.ISO601) = %v, want %v", gotFormat, wantFormat)
	}
}

func TestParseUnix(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		wantT   time.Time
		wantErr bool
	}{
		{
			name:  "with seconds",
			args:  args{input: "1656928751"},
			wantT: time.Date(2022, 7, 4, 9, 59, 11, 0, time.UTC),
		},
		{
			name:  "with milliseconds",
			args:  args{input: "1656928751873"},
			wantT: time.Date(2022, 7, 4, 9, 59, 11, 0, time.UTC),
		},
		{
			name:    "error bad input",
			args:    args{input: "165692875"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotT, err := datetime.ParseUnix(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseUnix() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil {
				return
			}
			if !reflect.DeepEqual(gotT, tt.wantT) {
				t.Errorf("ParseUnix() = %v, want %v", gotT, tt.wantT)
			}
		})
	}
}

func TestFormatUnix(t *testing.T) {
	type args struct {
		t time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "with milliseconds",
			want: "1656928751",
			args: args{t: time.Date(2022, 7, 4, 9, 59, 11, 0, time.UTC)},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := datetime.FormatUnix(tt.args.t); got != tt.want {
				t.Errorf("FormatUnix() = %v, want %v", got, tt.want)
			}
		})
	}
}
